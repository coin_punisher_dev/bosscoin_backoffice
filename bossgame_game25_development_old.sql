--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.active_admin_comments (
    id bigint NOT NULL,
    namespace character varying,
    body text,
    resource_type character varying,
    resource_id bigint,
    author_type character varying,
    author_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


ALTER TABLE public.active_admin_comments OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_admin_comments_id_seq OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.active_admin_comments_id_seq OWNED BY public.active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    role character varying DEFAULT 'supervisor'::character varying NOT NULL,
    avatar character varying
);


ALTER TABLE public.admin_users OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_users_id_seq OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.admin_users_id_seq OWNED BY public.admin_users.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO postgres;

--
-- Name: game_configurations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.game_configurations (
    id bigint NOT NULL,
    amount_coinbonus character varying,
    maxcoin_bonus character varying,
    description character varying
);


ALTER TABLE public.game_configurations OWNER TO postgres;

--
-- Name: game_configurations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.game_configurations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_configurations_id_seq OWNER TO postgres;

--
-- Name: game_configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.game_configurations_id_seq OWNED BY public.game_configurations.id;


--
-- Name: get_coin_from_spesific_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.get_coin_from_spesific_items (
    id bigint NOT NULL,
    item_name character varying NOT NULL,
    earn_coin character varying DEFAULT '0'::character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    is_active character varying DEFAULT 'No'::character varying
);


ALTER TABLE public.get_coin_from_spesific_items OWNER TO postgres;

--
-- Name: get_coin_from_spesific_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.get_coin_from_spesific_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.get_coin_from_spesific_items_id_seq OWNER TO postgres;

--
-- Name: get_coin_from_spesific_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.get_coin_from_spesific_items_id_seq OWNED BY public.get_coin_from_spesific_items.id;


--
-- Name: members; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.members (
    id bigint NOT NULL,
    game_code character varying NOT NULL,
    exp integer DEFAULT 0,
    coins integer DEFAULT 0
);


ALTER TABLE public.members OWNER TO postgres;

--
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.members_id_seq OWNER TO postgres;

--
-- Name: members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.members_id_seq OWNED BY public.members.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: variables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.variables (
    id bigint NOT NULL,
    key character varying,
    value character varying
);


ALTER TABLE public.variables OWNER TO postgres;

--
-- Name: variables_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.variables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.variables_id_seq OWNER TO postgres;

--
-- Name: variables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.variables_id_seq OWNED BY public.variables.id;


--
-- Name: active_admin_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.active_admin_comments ALTER COLUMN id SET DEFAULT nextval('public.active_admin_comments_id_seq'::regclass);


--
-- Name: admin_users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users ALTER COLUMN id SET DEFAULT nextval('public.admin_users_id_seq'::regclass);


--
-- Name: game_configurations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game_configurations ALTER COLUMN id SET DEFAULT nextval('public.game_configurations_id_seq'::regclass);


--
-- Name: get_coin_from_spesific_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.get_coin_from_spesific_items ALTER COLUMN id SET DEFAULT nextval('public.get_coin_from_spesific_items_id_seq'::regclass);


--
-- Name: members id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.members ALTER COLUMN id SET DEFAULT nextval('public.members_id_seq'::regclass);


--
-- Name: variables id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.variables ALTER COLUMN id SET DEFAULT nextval('public.variables_id_seq'::regclass);


--
-- Data for Name: active_admin_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.active_admin_comments (id, namespace, body, resource_type, resource_id, author_type, author_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin_users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at, role, avatar) FROM stdin;
2	onetechlabsbymuhammadridwan@gmail.com	$2a$11$fDO1fFcEGlbNqoqPwyn8dOkSA.D3y6inGOLbe5Hl1Z.yzufsqIFTO	\N	\N	\N	2020-06-08 21:17:41.328042	2020-06-08 21:17:41.328042	admin	\N
3	lovianatta@gmail.com	$2a$11$y5ozpoNgX2ORFhoVICj1wumJGDw370dJ5AXh/Ar.C9GJjWV2RCp5O	\N	\N	\N	2020-06-08 21:17:41.491382	2020-06-08 21:17:41.491382	supervisor	\N
\.


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2020-04-20 11:04:18.229418	2020-06-05 12:51:48.42767
\.


--
-- Data for Name: game_configurations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.game_configurations (id, amount_coinbonus, maxcoin_bonus, description) FROM stdin;
1	1	20	When Offline and Every 30 Minutes
2	1	10	When Online and Every 2 Minutes
\.


--
-- Data for Name: get_coin_from_spesific_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.get_coin_from_spesific_items (id, item_name, earn_coin, created_at, updated_at, is_active) FROM stdin;
13	BIG RED CHEST	1000000	2020-04-22 00:51:37.577084	2020-06-05 13:53:03.933415	Yes
12	RED STAFF	100000	2020-04-20 14:39:59.596506	2020-06-05 13:53:09.5213	Yes
11	RED SWORD	50000	2020-04-20 14:39:25.075664	2020-06-05 13:53:15.176466	Yes
10	RED SHIELD	25000	2020-04-20 14:34:48.138273	2020-06-05 13:53:20.056373	Yes
9	RED PRESENT	10000	2020-04-20 14:33:36.093567	2020-06-05 13:53:27.249157	Yes
8	RED DRAGON	7777	2020-04-20 14:32:56.554582	2020-06-05 13:53:33.074947	Yes
7	GREEN WALL COIN	5000	2020-04-20 14:31:01.94571	2020-06-05 13:53:37.629211	Yes
6	BLUE COIN	2500	2020-04-20 14:30:10.689968	2020-06-05 13:53:42.784136	Yes
5	GOLD ROCKET COIN	1000	2020-04-20 14:29:29.101063	2020-06-05 13:53:48.477768	Yes
4	PURPLE COIN	500	2020-04-20 14:28:43.323109	2020-06-05 13:53:56.816636	Yes
3	GOLD CLOVER COIN	100	2020-04-20 14:27:01.321696	2020-06-05 13:54:04.061197	Yes
2	SILVER SAND COIN	50	2020-04-20 14:26:11.116707	2020-06-05 13:54:17.658601	Yes
14	BIG RED CROWN	5000000	2020-04-22 00:52:07.125406	2020-06-05 20:44:39.970627	Yes
1	M GOLD COIN	10	2020-04-20 14:14:07.851471	2020-06-08 21:22:16.982567	Yes
15	WATER LILY	800	2020-04-22 00:52:07.125406	2020-06-05 20:44:39.970627	Yes
16	HORSE	1200	2020-04-22 00:52:07.125406	2020-06-05 20:44:39.970627	Yes
17	FLOWER	2800	2020-04-22 00:52:07.125406	2020-06-05 20:44:39.970627	Yes
18	FLASH	3300	2020-04-22 00:52:07.125406	2020-06-05 20:44:39.970627	Yes
\.


--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.members (id, game_code, exp, coins) FROM stdin;
11	111333	0	0
21	652739	4	9
129	70354698	7	39
101	28056341	5	0
27	576840	17	0
69	37820945	12	0
66	98467890	7	0
62	91623570	7	0
166	26841507	26	0
30	631457	5	0
82	976304	6	190
53	412730	4	4
45	03148952	4	0
37	97513028	4	0
26	579603	9	0
49	628495	9	191
84	59283761	11	0
137	50782394	8	118
68	63120874	7	40
76	85120394	16	0
75	68417953	9	0
39	678531	7	0
133	78206495	7	0
99	95302147	17	0
28	860794	5	0
16	497125	4	0
47	72045938	3	0
112	87349065	6	0
41	895703	6	0
17	04573296	4	0
126	83019245	4	0
161	15607439	6	0
168	96324150	7	50
48	54628103	0	0
35	64985270	9	0
98	045287	7	0
123	073149	9	0
85	83521907	8	0
50	350469	10	0
115	13462095	11	0
29	52974368	12	0
118	23187690	10	0
36	293764	8	0
102	275910	0	177
64	751894	42	100
23	873026	17	0
109	79130548	19	0
44	647102	19	7
135	45102789	8	200
113	40563798	6	216
79	62039578	17	60
88	88746351901	3	18
116	04357891	4	21
65	08391765	118	50
55	40862915	15	0
111	10937846	15	0
70	54976318	21	60
54	34971086	5	0
132	28930517	9	0
38	674139	5	40
72	92831675	30	0
78	72965481	17	0
131	419378	5	20
104	95623710	6	200
33	07234681	7	0
136	87649021	6	20
32	07436859	19	0
91	10782569	11	0
15	493671	8	20
103	72893014	9	60
18	24960751	3	10
12	614052	30	0
56	85194630	11	0
120	94078521	4	100
87	54632810	15	205
122	14369072	4	200
171	32718409	5	130
124	25871396	9	0
100	20657891	6	0
95	05823167	9	0
52	469312	4	0
74	406781	16	0
96	60538124	8	0
46	54390728	75	209
73	03967412	19	40
89	31625079	5	83
58	210384	9	0
25	216384	9	140
134	62148039	7	0
77	48206391	5	0
117	98430157	3	152
42	27356941	6	0
108	18694375	3	20
119	94132856	6	0
130	09471268	16	0
31	91346587	4	0
121	73089251	1	170
94	84617235	7	0
13	503781	16	0
19	680413	3	126
63	45076819	4	724
24	856174	7	0
178	27086513	12	60
51	415327	0	0
93	179038	7	90
92	13976205	15	40
140	49685301	12	0
57	720894	5	0
128	76941023	7	200
61	451890	15	0
86	52981046	15	100
67	49625831	5	10
110	50829761	4	7
59	824175	32	0
14	718296	11	94
106	34870162	6	0
127	91276038	1	0
97	16258374	40	0
163	79326148	6	0
138	56183024	6	20
40	99112234	4	43
125	46702351	4	127
164	25438061	14	30
22	97062851	10	193
43	26450981	9	500
114	074635	9	0
107	64983015	18	0
202	51703694	6	20
183	54673280	8	224
170	79305641	7	108
177	02679485	4	0
173	76041395	7	0
167	36280754	8	0
190	12543796	4	0
172	47610235	22	10
184	06274513	4	0
150	56238917	7	0
188	21784659	7	56
152	86273419	7	16
142	69501478	6	200
165	86245907	11	0
212	38160524	7	262
203	76041859	6	200
81	65790382	234	58
141	78787899012	5	0
209	12548607	4	0
149	98137245	4	0
34	49170863	13	225
179	20589614	4	0
153	80674321	12	0
181	53671940	0	80
160	79453218	6	0
154	28365071	4	0
200	73526019	2	0
158	85924036	7	0
157	82537046	4	0
205	82431596	6	100
139	26591843	6	88
146	28013974	10	45
185	93807546	5	263
80	71298346	14	37
156	82613745	5	20
210	65948237	3	433
215	64709382	3	20
176	75184329	18	100
208	52049183	2	0
60	402381	11	40
151	69785132	4	10
144	878787091222	4	285
187	321067	4	0
147	18063972	9	22
155	12856374	13	95
105	14639250	7	0
148	58607349	10	0
159	84120579	6	0
189	29837015	6	100
207	05293476	5	5
162	74823915	4	0
186	79612340	5	40
71	30961574	95	97
180	92804713	6	200
211	83597416	6	200
145	378065	4	0
182	40157238	5	0
201	27086513 	3	100
213	89572461	4	0
197	47068915	9	13
192	02613958	3	20
169	49137528	17	0
204	13427568	5	20
199	94632801	5	80
90	73062945	18	200
193	72053189	3	100
143	12495703	9	20
83	75134268	63	0
191	86245071	4	94
214	54926318	2	0
195	92456718	3	40
175	05984216	4	200
174	65703812	7	0
194	14738059	4	0
206	91780265	2	100
196	86719324	6	100
198	02814536	7	300
20	856210	6	9
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schema_migrations (version) FROM stdin;
20200402045912
20200402045914
20200402052203
20200404035033
20200407033305
20200407034539
20200407034820
20200407035012
20200416091743
20200416102851
20200416103555
20200422103212
20200422104744
20200422104811
20200605055018
20200605061205
20200605064018
20200605064153
\.


--
-- Data for Name: variables; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.variables (id, key, value) FROM stdin;
2	inStartGame_versionGame	1.0.6
1	inStartGame_isMaintenance	false
\.


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.active_admin_comments_id_seq', 1, false);


--
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.admin_users_id_seq', 3, true);


--
-- Name: game_configurations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.game_configurations_id_seq', 1, false);


--
-- Name: get_coin_from_spesific_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.get_coin_from_spesific_items_id_seq', 14, true);


--
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.members_id_seq', 215, true);


--
-- Name: variables_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.variables_id_seq', 4, true);


--
-- Name: active_admin_comments active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: game_configurations game_configurations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game_configurations
    ADD CONSTRAINT game_configurations_pkey PRIMARY KEY (id);


--
-- Name: get_coin_from_spesific_items get_coin_from_spesific_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.get_coin_from_spesific_items
    ADD CONSTRAINT get_coin_from_spesific_items_pkey PRIMARY KEY (id);


--
-- Name: members members_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: variables variables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.variables
    ADD CONSTRAINT variables_pkey PRIMARY KEY (id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON public.active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_active_admin_comments_on_namespace ON public.active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON public.active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX index_admin_users_on_email ON public.admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON public.admin_users USING btree (reset_password_token);


--
-- Name: index_members_on_game_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX index_members_on_game_code ON public.members USING btree (game_code);


--
-- PostgreSQL database dump complete
--

