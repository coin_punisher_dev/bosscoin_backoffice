class CreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :members do |t|
      t.string :game_code, null: false
      t.string :coins, null: false, default: "0"
    end
    add_index :members, :game_code, unique: true
  end
end
