class AddRulesShowingToGetCoinFromSpesificItems < ActiveRecord::Migration[6.0]
  def change
    add_column :get_coin_from_spesific_items, :show_when_min_exp, :integer, null: true
    add_column :get_coin_from_spesific_items, :show_when_min_coin, :integer, null: true
  end
end
