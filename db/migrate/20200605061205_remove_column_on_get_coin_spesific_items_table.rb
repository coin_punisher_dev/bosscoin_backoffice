class RemoveColumnOnGetCoinSpesificItemsTable < ActiveRecord::Migration[6.0]
  def up
    remove_column :get_coin_from_spesific_items, :show_when_min_exp
    remove_column :get_coin_from_spesific_items, :show_when_min_coin
    remove_column :get_coin_from_spesific_items, :status_auto_set
    remove_column :get_coin_from_spesific_items, :set_to_gamecode
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
