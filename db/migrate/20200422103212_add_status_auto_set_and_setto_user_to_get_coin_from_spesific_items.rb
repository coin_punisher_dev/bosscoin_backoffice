class AddStatusAutoSetAndSettoUserToGetCoinFromSpesificItems < ActiveRecord::Migration[6.0]
  def change
    add_column :get_coin_from_spesific_items, :status_auto_set, :string, default: "No"
    add_column :get_coin_from_spesific_items, :set_to_gamecode, :string
  end
end
