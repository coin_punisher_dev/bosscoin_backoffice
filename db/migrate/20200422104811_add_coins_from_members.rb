class AddCoinsFromMembers < ActiveRecord::Migration[6.0]
  def change
  	add_column :members, :coins, :integer, default: 0
  end
end
