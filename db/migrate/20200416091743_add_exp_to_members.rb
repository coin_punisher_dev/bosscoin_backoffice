class AddExpToMembers < ActiveRecord::Migration[6.0]
  def change
    add_column :members, :exp, :integer, default: 0
  end
end
