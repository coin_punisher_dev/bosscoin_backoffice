class CreateHistoryTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :history_transactions do |t|
      t.text :message, null: false
      t.string :game_code, null: false
      t.timestamps null: false
    end
  end
end
