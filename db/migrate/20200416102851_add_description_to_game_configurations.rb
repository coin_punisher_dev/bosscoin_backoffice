class AddDescriptionToGameConfigurations < ActiveRecord::Migration[6.0]
  def change
    add_column :game_configurations, :description, :string, null: true
  end
end
