class CreateGetCoinFromSpesificItems < ActiveRecord::Migration[6.0]
  def change
    create_table :get_coin_from_spesific_items do |t|
      t.string :item_name, null: false
      t.string :earn_coin, default: "0"
      t.timestamps null: false
    end
  end
end
