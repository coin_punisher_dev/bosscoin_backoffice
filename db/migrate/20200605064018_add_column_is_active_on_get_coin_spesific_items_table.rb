class AddColumnIsActiveOnGetCoinSpesificItemsTable < ActiveRecord::Migration[6.0]
  def up
    add_column :get_coin_from_spesific_items, :is_active, :string, default: "No"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
