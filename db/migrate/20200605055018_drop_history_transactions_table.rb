class DropHistoryTransactionsTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :history_transactions
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
