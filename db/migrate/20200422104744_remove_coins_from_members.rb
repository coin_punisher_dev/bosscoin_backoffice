class RemoveCoinsFromMembers < ActiveRecord::Migration[6.0]
  def change

    remove_column :members, :coins, :string
  end
end
