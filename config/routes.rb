Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/', to: redirect('/admin')
  post 'api/login' , to: 'application#login'
  post 'api/login-by-email' , to: 'application#loginByEmail'
  get 'api/itemlist' , to: 'application#itemlist'
  get 'api/coinsetting' , to: 'application#coinsetting'
  post 'api/user/increase/exp' , to: 'application#user_increase_exp'
  get 'api/leaderboard/exp' , to: 'application#leaderboard_exp'
  get 'api/leaderboard/coins' , to: 'application#leaderboard_coins'
  get 'api/vars' , to: 'application#variables'
end
