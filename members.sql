--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.members (id, game_code, exp, coins) VALUES (11, '111333', 0, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (21, '652739', 4, 9);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (104, '95623710', 7, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (101, '28056341', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (14, '718296', 13, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (69, '37820945', 12, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (36, '293764', 13, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (62, '91623570', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (166, '26841507', 26, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (30, '631457', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (82, '976304', 6, 190);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (53, '412730', 4, 4);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (45, '03148952', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (37, '97513028', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (26, '579603', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (140, '49685301', 12, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (89, '31625079', 6, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (20, '856210', 4, 14);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (13, '503781', 18, 6901);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (76, '85120394', 16, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (75, '68417953', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (123, '073149', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (133, '78206495', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (29, '52974368', 13, 120);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (28, '860794', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (16, '497125', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (47, '72045938', 3, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (112, '87349065', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (41, '895703', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (17, '04573296', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (87, '54632810', 16, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (126, '83019245', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (161, '15607439', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (12, '614052', 31, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (48, '54628103', 0, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (35, '64985270', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (68, '63120874', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (73, '03967412', 20, 131);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (103, '72893014', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (50, '350469', 10, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (138, '56183024', 12, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (49, '628495', 9, 13);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (118, '23187690', 10, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (99, '95302147', 17, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (102, '275910', 0, 177);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (59, '824175', 32, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (23, '873026', 17, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (109, '79130548', 19, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (55, '40862915', 16, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (86, '52981046', 15, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (85, '83521907', 9, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (88, '88746351901', 3, 18);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (116, '04357891', 4, 21);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (113, '40563798', 7, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (168, '96324150', 10, 71);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (111, '10937846', 15, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (134, '62148039', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (54, '34971086', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (132, '28930517', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (38, '674139', 5, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (72, '92831675', 30, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (78, '72965481', 17, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (106, '34870162', 16, 38);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (15, '493671', 8, 80);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (178, '27086513', 20, 19);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (33, '07234681', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (136, '87649021', 6, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (51, '415327', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (91, '10782569', 11, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (125, '46702351', 4, 51);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (44, '647102', 26, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (18, '24960751', 3, 10);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (61, '451890', 16, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (56, '85194630', 11, 80);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (120, '94078521', 4, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (108, '18694375', 4, 16);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (39, '678531', 9, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (64, '751894', 44, 79);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (124, '25871396', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (100, '20657891', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (95, '05823167', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (52, '469312', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (74, '406781', 16, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (96, '60538124', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (130, '09471268', 23, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (25, '216384', 11, 140);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (163, '79326148', 7, 50);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (58, '210384', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (129, '70354698', 7, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (32, '07436859', 19, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (77, '48206391', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (117, '98430157', 3, 152);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (42, '27356941', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (65, '08391765', 153, 55);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (79, '62039578', 18, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (119, '94132856', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (122, '14369072', 5, 39);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (31, '91346587', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (121, '73089251', 1, 170);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (94, '84617235', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (66, '98467890', 7, 88);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (19, '680413', 3, 126);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (63, '45076819', 4, 724);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (24, '856174', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (135, '45102789', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (27, '576840', 20, 25);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (93, '179038', 7, 90);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (92, '13976205', 15, 119);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (171, '32718409', 6, 400);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (57, '720894', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (46, '54390728', 81, 433);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (84, '59283761', 11, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (70, '54976318', 23, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (67, '49625831', 5, 10);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (110, '50829761', 4, 7);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (97, '16258374', 41, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (98, '045287', 7, 60);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (131, '419378', 6, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (127, '91276038', 1, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (115, '13462095', 20, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (137, '50782394', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (43, '26450981', 10, 140);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (128, '76941023', 13, 190);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (40, '99112234', 4, 43);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (164, '25438061', 14, 30);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (22, '97062851', 10, 193);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (114, '074635', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (107, '64983015', 18, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (212, '38160524', 7, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (205, '82431596', 8, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (170, '79305641', 7, 108);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (83, '75134268', 69, 362);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (173, '76041395', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (216, '64037291', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (190, '12543796', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (147, '18063972', 9, 22);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (184, '06274513', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (150, '56238917', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (152, '86273419', 7, 16);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (165, '86245907', 11, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (174, '65703812', 12, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (200, '73526019', 2, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (71, '30961574', 102, 68);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (141, '78787899012', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (209, '12548607', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (81, '65790382', 267, 28);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (60, '402381', 13, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (218, '89427105', 3, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (220, '03769241', 4, 60);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (179, '20589614', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (153, '80674321', 12, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (177, '02679485', 10, 120);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (160, '79453218', 6, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (154, '28365071', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (158, '85924036', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (157, '82537046', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (144, '878787091222', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (204, '13427568', 7, 21);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (213, '89572461', 8, 67);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (156, '82613745', 5, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (195, '92456718', 4, 80);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (183, '54673280', 14, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (159, '84120579', 7, 84);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (208, '52049183', 2, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (151, '69785132', 4, 10);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (214, '54926318', 19, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (139, '26591843', 6, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (149, '98137245', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (203, '76041859', 11, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (196, '86719324', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (162, '74823915', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (199, '94632801', 8, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (206, '91780265', 3, 36);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (181, '53671940', 1, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (167, '36280754', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (175, '05984216', 5, 220);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (90, '73062945', 18, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (105, '14639250', 9, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (146, '28013974', 10, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (180, '92804713', 6, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (145, '378065', 4, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (221, '31095827', 2, 300);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (197, '47068915', 9, 21);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (201, '27086513 ', 3, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (223, '84376290', 0, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (143, '12495703', 10, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (198, '02814536', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (176, '75184329', 21, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (182, '40157238', 14, 3);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (169, '49137528', 17, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (217, '184793', 0, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (172, '47610235', 53, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (192, '02613958', 3, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (148, '58607349', 10, 60);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (193, '72053189', 3, 100);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (215, '64709382', 5, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (34, '49170863', 13, 90);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (210, '65948237', 5, 210);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (142, '69501478', 6, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (211, '83597416', 6, 92);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (222, '51894026', 3, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (191, '86245071', 4, 74);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (219, '10548627', 0, 200);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (189, '29837015', 8, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (187, '321067', 7, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (155, '12856374', 13, 107);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (202, '51703694', 6, 20);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (185, '93807546', 5, 255);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (186, '79612340', 7, 40);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (188, '21784659', 7, 82);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (224, '18405962', 5, 0);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (194, '14738059', 14, 138);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (207, '05293476', 6, 49);
INSERT INTO public.members (id, game_code, exp, coins) VALUES (80, '71298346', 14, 0);


--
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.members_id_seq', 224, true);


--
-- PostgreSQL database dump complete
--

