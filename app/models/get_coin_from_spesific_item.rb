class GetCoinFromSpesificItem < ApplicationRecord
  validates :item_name, presence: true, uniqueness: true
  validates :earn_coin, presence: true
end
