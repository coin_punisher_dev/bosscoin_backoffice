class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new

    if user.role == 'admin'
      can :manage, :all
    elsif user.role == 'supervisor'
      can :read, :all
      can [:read, :create], ActiveAdmin::Comment
    end
  end
end