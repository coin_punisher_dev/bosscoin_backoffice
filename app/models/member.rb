class Member < ApplicationRecord
	validates :game_code, presence: true, uniqueness: true
	validates :coins, presence: true
	validates :exp, presence: true
end
