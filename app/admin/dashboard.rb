ActiveAdmin.register_page "Dashboard" do

  menu label: "<i class='fa fa-window-restore'></i> Dashboard".html_safe, priority: 1


    content title: "Dashboard" do
        div class: "blank_slate_container", id: "dashboard_default_message" do
          span class: "blank_slate" do
            div style: "text-align:center" do
              h1 "Welcome to Boss Coin"
              small "Hii ".html_safe+current_admin_user.email+" ( ".html_safe+current_admin_user.role+ " )".html_safe
            end
            br
            br
          end
        end
        columns do
            column do
                panel "Summary" do
                    div class: "paginated_collection" do
                        div class: "paginated_collection_contents" do
                            table do
                                thead do
                                    tr do
                                        th class: 'col' do
                                            'Counter Coin Position'
                                        end
                                        th class: 'col' do
                                            'Total Member Registered'
                                        end
                                    end
                                end
                                tbody do
                                    tr do
                                        td style: 'font-size:20px' do
                                          loginBossgameCoin = RestClient.post $bossgameAPI+'loginGameCoin.php', {'kode': "856210", 'idGame' => "25"}
                                      		parsed_json = ActiveSupport::JSON.decode(loginBossgameCoin)
                                          parsed_json['sort'].to_s
                                        end
                                        td style: 'font-size:20px' do
                                            memC = Member.count
                                            memC.to_s + " Members"
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        columns do
            column do
                panel "(Leaderboard) 30 Top Rich Players" do
                    div class: "paginated_collection" do
                        div class: "paginated_collection_contents" do
                            table do
                                thead do
                                    tr do
                                        th class: 'col' do
                                            'Game Code'
                                        end
                                        th class: 'col' do
                                            'Coin Earned'
                                        end
                                    end
                                end
                                tbody do
                                    topRichPlayer=Member.all.order(coins: :desc).limit(30)
                                    topRichPlayer.each do |e|
                                        tr do
                                            td do
                                                e.game_code
                                            end
                                            td do
                                                e.coins
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
            column do
                panel "(Leaderboard) 30 Top Game EXP Players" do
                    div class: "paginated_collection" do
                        div class: "paginated_collection_contents" do
                            table do
                                thead do
                                    tr do
                                        th class: 'col' do
                                            'Game Code'
                                        end
                                        th class: 'col' do
                                            'Game Experience (EXP)'
                                        end
                                    end
                                end
                                tbody do
                                    topRichPlayer=Member.all.order(exp: :desc).limit(30)
                                    topRichPlayer.each do |e|
                                        tr do
                                            td do
                                                e.game_code
                                            end
                                            td do
                                                e.exp
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        columns do
            column do
                panel "Member/Players List Orderd by Newest" do
                    player = Member.order(id: :desc)
                    paginated_collection(player.page(params[:dashboard_player_page]).per(10), { param_name: 'dashboard_player_page', download_links: false }) do
                        table_for(collection) do |cr|
                            column("Game Code") { |cr| cr.game_code }
                            column("Game EXP") { |cr| cr.exp }
                            column("Coin Earned") { |cr| cr.coins }
                        end
                    end
                end
            end
        end
    end # content
end
