ActiveAdmin.register GameConfiguration, as: "GameConfiguration" do
  before_filter :skip_sidebar!, :only => :index
  actions :all, :except => [:destroy]
  permit_params :amount_coinbonus, :maxcoin_bonus, :description
  config.action_items.delete_if { true }
  menu parent: "<i class='fa fa-cog'></i> General Settings".html_safe , label: "<i class='fa fa-gamepad'></i> Bonus Controller".html_safe , priority: 5, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }

  amount_coinbonus = "Amount (Coin) Bonus"
  maxcoin_bonus = "Max (Coin) Bonus "
  description = "Description"

  controller do
    def edit
      @page_title = "Edit Game Configuration"
      super
    end

    def update
      update! { |success, failure|
       success.html do
         redirect_to "/admin/game_configurations", :notice => "Game Configuration has been Saved"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/game_configurations"
       end
      }
    end

  end

  index do
    column amount_coinbonus, :amount_coinbonus
    column maxcoin_bonus, :maxcoin_bonus
    column description, :description
    column :actions do |resource|
      div class: "table_actions" do
        links = link_to 'View', resource_path(resource), class: 'view_link member_link'
        if resource.id == 2 then
          links += link_to 'Edit', edit_resource_path(resource), class: 'edit_link member_link'
        end
        links
      end
    end
  end

  show do
    attributes_table do
      row amount_coinbonus do |r|
        r.amount_coinbonus
      end
      row maxcoin_bonus do |r|
        r.maxcoin_bonus
      end
      row description do |r|
        r.description
      end
    end
    active_admin_comments
  end

  form do |f|
    f.inputs :multipart => true do
      f.input :amount_coinbonus,  :label => amount_coinbonus
      f.input :maxcoin_bonus,  :label => maxcoin_bonus
      f.input :description,  :label => description
    end
    f.actions do
      if resource.persisted?
        # edit
        f.action :submit, label: 'Save This Configuration'
      end
      f.action :cancel, as: :link, label: 'Cancel'
    end
  end

end
