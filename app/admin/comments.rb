ActiveAdmin.register ActiveAdmin::Comment, as: "UserComment" do
  menu parent: "<i class='fa fa-users'></i> User Management".html_safe , label: "<i class='fa fa-envelope-open'></i> User Comments".html_safe , priority: 4, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }
  config.action_items.delete_if { true }
  author = "Author"
  body = "Body Message"
  created_at = "Commented at"

  filter :body, as: :string,  :label => body

  index do
    if current_admin_user.role == "admin" then
      selectable_column
    end
    column author do |r|
      AdminUser.find_by(id: r.author_id).email
    end
    column body, :body
    column created_at, :created_at
  end

end