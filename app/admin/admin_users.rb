ActiveAdmin.register AdminUser, as: "UserData" do

  permit_params :avatar, :role, :email, :password, :password_confirmation
  menu parent: "<i class='fa fa-users'></i> User Management".html_safe , label: "<i class='fa fa-address-book'></i> Users".html_safe, priority: 2, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }

  avatar = "Profile Picture"
  email = "Email Address"
  role = "Role Status"
  created_at = "Registered at"
  updated_at = "Modified at"
  password = "Enter Password"
  password_confirmation = "Confirmation Password"

  controller do

    def scoped_collection
      if current_admin_user.role == "admin" then
        super.all
      else
        super.where(role: "supervisor")
      end
    end

    def new
      @page_title = "Create new User"
      super
    end

    def edit
      usr = AdminUser.find(URI(request.path).path.split('/')[3])
      @page_title = usr.email
      super
    end

    def create
      if File.extname(params[:admin_user]["avatar"])==".jpg" || File.extname(params[:admin_user]["avatar"])==".jpeg" || File.extname(params[:admin_user]["avatar"])==".png" then
      else
        if params[:admin_user]["avatar"].empty? then
        else
          avatar_binary = params[:admin_user]["avatar"].split(";base64,")
          params[:admin_user]["avatar"] = Time.now.strftime("%d%m%Y%H%M")+rand(10..10000).to_s+"-avatar"+".png"
          File.open(Rails.root.to_s + "/public/uploads/avatars/"+params[:admin_user]["avatar"], "wb") do |file|
            file.write(Base64.decode64(avatar_binary[1]))
          end
        end
      end
      create! { |success, failure|
       success.html do
         redirect_to "/admin/user_data", :notice => "New User Sucessfully Created"
       end
       failure.html do
         flash[:error] = "Something Wrong(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/user_data"
       end
      }
    end

    def update
      if File.extname(params[:admin_user]["avatar"])==".jpg" || File.extname(params[:admin_user]["avatar"])==".jpeg" || File.extname(params[:admin_user]["avatar"])==".png" then
      else
        if params[:admin_user]["avatar"].empty? then
        else
          avatar_binary = params[:admin_user]["avatar"].split(";base64,")
          params[:admin_user]["avatar"] = Time.now.strftime("%d%m%Y%H%M")+rand(10..10000).to_s+"-avatar"+".png"
          File.open(Rails.root.to_s + "/public/uploads/avatars/"+params[:admin_user]["avatar"], "wb") do |file|
            file.write(Base64.decode64(avatar_binary[1]))
          end
        end
      end
      update! { |success, failure|
       success.html do
         redirect_to "/admin/user_data", :notice => "New User Data has been Saved"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/user_data"
       end
      }
    end

    def destroy
      destroy! { |success, failure|
       success.html do
         redirect_to "/admin/user_data", :notice => "User Data has been Destroyed"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/user_data"
       end
      }
    end

    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end

  end

  breadcrumb do
    links = [link_to('Admin', admin_root_path)]

    if %(edit).include?(params['action'])
      links << link_to("Users", admin_root_path+"/user_data")
    elsif %(show).include?(params['action'])
      links << link_to("Users", admin_root_path+"/user_data")
    end

    links
  end

  index :title => 'Users' do
    selectable_column
    column email, :email
    column role, :role
    column created_at, :created_at
    actions
  end

  show do
    attributes_table do
      row avatar do |r|
        avatar_src = r.avatar
        if avatar_src == nil || avatar_src == "" then
          "No "+avatar+" Found !".html_safe
        else
          image_tag("/uploads/avatars/"+avatar_src, :class => "img-thumbnail", :style => 'width:200px;')
        end
      end
      row email do |r|
        r.email
      end
      row role do |r|
        r.role
      end
      row created_at do |r|
        r.created_at
      end
      row updated_at do |r|
        r.updated_at
      end
    end
    active_admin_comments
  end

  filter :email,  :label => email
  filter :role,  :label => role,
         as: :select,
         collection: { 'Admin' => 'admin', 'Supervisor' => 'supervisor' },
         include_blank: false
  filter :created_at,  :label => created_at

  form do |f|
    f.inputs :multipart => true do
      if f.object.avatar then
        f.li f.object.avatar == nil || f.object.avatar !="" ? image_tag("/uploads/avatars/"+f.object.avatar, :class => "img-thumbnail section_oldAvatar", :style => 'width:200px;') : content_tag(:span, ""), :style => 'width:100%; text-align: center; margin-bottom:20px;'
      end

      f.li content_tag(:span,
        content_tag(:div, '
          <center>
          <div class="section_newAvatar" id="views"></div>
          </center>
          <input class="section_oldAvatar" id="avatar_upload_file" type="file">
          <br class="section_newAvatar"/>
          <button class="section_newAvatar" id="cropbutton" type="button">Crop</button>
          <button class="section_newAvatar" id="scalebutton" type="button">Scale</button>
          <button class="section_newAvatar" id="rotatebutton" type="button">Rotate</button>
          <button class="section_newAvatar" id="hflipbutton" type="button">H-flip</button>
          <button class="section_newAvatar" id="vflipbutton" type="button">V-flip</button>
          <br class="section_newAvatar"/>
        '.html_safe), :class => 'image-cropper'
      ), :style => 'width:100%; text-align: center; margin-bottom:20px;'
      f.input :avatar, as: :hidden,  :label => avatar
      f.input :role,
              label: role,
              as: :select,
              collection: { 'Admin' => 'admin', 'Supervisor' => 'supervisor' },
              include_blank: false,
              :input_html => {:style => "padding: 6px 12px;font-size: 14px;line-height: 1.57142857;color: #323537;"}
      f.input :email,  :label => email
      f.input :password,  :label => password
      f.input :password_confirmation,  :label => password_confirmation
    end
    f.actions do
      if resource.persisted?
        # edit
        f.action :submit, label: 'Save User Data'
      else
        # new
        f.action :submit, label: 'Create new User'
      end
      f.action :cancel, as: :link, label: 'Cancel'
    end
  end

end
