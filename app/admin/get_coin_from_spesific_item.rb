ActiveAdmin.register GetCoinFromSpesificItem, as: "GetCoinFromSpesificItem" do
  permit_params :item_name, :earn_coin, :is_active
  menu parent: "<i class='fa fa-cog'></i> General Settings".html_safe , label: "<i class='fa fa-gift'></i> Items".html_safe , priority: 6, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }

  item_name = "Item Name"
  earn_coin = "Coin will Earned"
  is_active = "Is Active ?"
  created_at = "Created at"
  updated_at = "Modified at"

  controller do
    def new
      @page_title = "Create Coin Item"
      super
    end

    def edit
      @page_title = "Edit Coin Item"
      super
    end

    def create
      create! { |success, failure|
       success.html do
         redirect_to "/admin/get_coin_from_spesific_items", :notice => "Coin Item Sucessfully Created"
       end
       failure.html do
         flash[:error] = "Something Wrong(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/get_coin_from_spesific_items"
       end
      }
    end

    def update
      update! { |success, failure|
       success.html do
         redirect_to "/admin/get_coin_from_spesific_items", :notice => "Coin Item has been Saved"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/get_coin_from_spesific_items"
       end
      }
    end

    def destroy
      super do |format|
        flash[:notice] = "Coin Item has been Destroyed"
        return redirect_to "/admin/get_coin_from_spesific_items"
      end
    end

  end

  index do
    if current_admin_user.role == "admin" then
      selectable_column
    end
    column item_name, :item_name
    column is_active, :is_active
    actions
  end

  show do
    attributes_table do
      row item_name do |r|
        r.item_name
      end
      row earn_coin do |r|
        r.earn_coin
      end
      row is_active do |r|
        r.is_active
      end
      row created_at do |r|
        r.created_at
      end
      row updated_at do |r|
        r.updated_at
      end
    end
    active_admin_comments
  end

  filter :item_name, as: :string,  :label => item_name

  form do |f|
    f.inputs :multipart => true do
      f.input :item_name,  :label => item_name
      f.input :earn_coin,  :label => earn_coin
      f.input :is_active,
              label: is_active,
              as: :select,
              collection: { 'No' => 'No', 'Yes' => 'Yes' },
              include_blank: false,
              :input_html => {:style => "padding: 6px 12px;font-size: 14px;line-height: 1.57142857;color: #323537;"}
    end
    f.actions do
      if resource.persisted?
        # edit
        f.action :submit, label: 'Save Coin Item Data'
      else
        # new
        f.action :submit, label: 'Create new Coin Item'
      end
      f.action :cancel, as: :link, label: 'Cancel'
    end
  end

end
