ActiveAdmin.register Variable, as: "Variables" do
  permit_params :key, :value
  menu parent: "<i class='fa fa-cog'></i> General Settings".html_safe , label: "<i class='fa fa-code'></i> Variables".html_safe , priority: 6, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }

  key = "Key"
  value = "Value"

  controller do
    def new
      @page_title = "Create Variable"
      super
    end

    def edit
      @page_title = "Edit Variable"
      super
    end

    def create
      create! { |success, failure|
       success.html do
         redirect_to "/admin/variables", :notice => "Variable Sucessfully Created"
       end
       failure.html do
         flash[:error] = "Something Wrong(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/variables"
       end
      }
    end

    def update
      update! { |success, failure|
       success.html do
         redirect_to "/admin/variables", :notice => "Variable has been Saved"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/variables"
       end
      }
    end

    def destroy
      super do |format|
        flash[:notice] = "Variable has been Destroyed"
        return redirect_to "/admin/variables"
      end
    end

  end

  index do
    if current_admin_user.role == "admin" then
      selectable_column
    end
    column key, :key
    column value, :value
    actions
  end

  show do
    attributes_table do
      row key do |r|
        r.key
      end
      row value do |r|
        r.value
      end
    end
    active_admin_comments
  end

  form do |f|
    f.inputs :multipart => true do
      f.input :key,  :label => key
      f.input :value,  :label => value
    end
    f.actions
  end

end
