ActiveAdmin.register Member, as: "Member" do
  permit_params :exp, :game_code, :coins
  menu label: "<i class='fa fa-user'></i> Members".html_safe , priority: 3, if: proc { current_admin_user.role == "admin" || current_admin_user.role == "supervisor" }
  
  game_code = "Game Code"
  coins = "Coins"
  exp = "Game Experience (EXP)"

  controller do
    def new
      @page_title = "Create Member Information"
      super
    end

    def edit
      @page_title = "Edit Member Information"
      super
    end

    def create 
      create! { |success, failure|
       success.html do
         redirect_to "/admin/members", :notice => "Member Information Sucessfully Created"
       end
       failure.html do
         flash[:error] = "Something Wrong(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/members" 
       end
      }
    end

    def update 
      update! { |success, failure|
       success.html do
         redirect_to "/admin/members", :notice => "Member Information Data has been Saved"
       end
       failure.html do
         flash[:error] = "Error(s) : #{resource.errors.full_messages.join(', ')}"
         redirect_to "/admin/members" 
       end
      }
    end

    def destroy 
      super do |format| 
        flash[:notice] = "Member Information Data has been Destroyed"
        return redirect_to "/admin/members"
      end
    end
    
  end

  index do
    if current_admin_user.role == "admin" then
      selectable_column
    end
    column game_code, :game_code
    actions
  end

  show do
    attributes_table do
      row game_code do |r|
        r.game_code
      end
      row exp do |r|
        r.exp
      end
      row coins do |r|
        r.coins
      end
    end
    active_admin_comments
  end

  filter :game_code, as: :string,  :label => game_code
  filter :exp, as: :string,  :label => exp

  form do |f|
    f.inputs :multipart => true do 
      f.input :exp,  :label => exp
    end
    f.actions do
      if resource.persisted?
        # edit
        f.action :submit, label: 'Save Member Information Data'
      else
        # new
        f.action :submit, label: 'Create new Member Information'
      end
      f.action :cancel, as: :link, label: 'Cancel'
    end
  end

end
