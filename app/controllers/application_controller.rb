# frozen_string_literal: true
require 'rest-client'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  protect_from_forgery prepend: true
  include Rails::Pagination

  $bossgameAPI = "https://bossgame.co.id/webServices/"

  	def login
  		loginBossgameCoin = RestClient.post $bossgameAPI+'loginGameCoin.php', {'kode': params[:game_user_code], 'idGame' => params[:game_id]}
  		parsed_json = ActiveSupport::JSON.decode(loginBossgameCoin)
  		if parsed_json['value'] == 1 then
  			findUser = Member.where(game_code: parsed_json['memberID']).count
  			if findUser.to_i == 0 then
  				u = Member.new
  				u.coins = parsed_json['coin'].to_s
  				u.game_code = params[:game_user_code].to_s
  				u.exp = 0
  				u.save
  			elsif findUser.to_i >= 1 then
  				u = Member.find_by({ :game_code => params[:game_user_code] })
  				u.coins = parsed_json['coin'].to_s
  				u.save
  			end
  			gc = findUser.to_i
  			data_ingame = Member.find_by({ :game_code => params[:game_user_code] })
  			data_bossgame = parsed_json
  		elsif parsed_json['value'] == 2 then
  			gc = 0
  			data_ingame = []
  			data_bossgame = parsed_json
  		end
  		render json: [{'data_ingame':data_ingame ,'data_bossgame':data_bossgame, 'present': gc.to_i == 0 ? "no" : "yes"}]
	end
	
	def loginByEmail
  		loginBossgameCoin = RestClient.post $bossgameAPI+'loginGameCoinGooglePlay.php', {'email': params[:byemail], 'idGame' => params[:game_id]}
  		parsed_json = ActiveSupport::JSON.decode(loginBossgameCoin)
  		if parsed_json['value'] == 1 then
			findUser = Member.where(game_code: parsed_json['memberID']).count
  			if findUser.to_i == 0 then
  				u = Member.new
  				u.coins = parsed_json['coin'].to_s
  				u.game_code = parsed_json['memberID'].to_s
  				u.exp = 0
  				u.save
  			elsif findUser.to_i >= 1 then
  				u = Member.find_by({ :game_code => parsed_json['memberID'] })
  				u.coins = parsed_json['coin'].to_s
  				u.save
  			end
  			gc = findUser.to_i
  			data_ingame = Member.find_by({ :game_code => parsed_json['memberID'] })
  			data_bossgame = parsed_json
  		elsif parsed_json['value'] == 2 then
  			gc = 0
  			data_ingame = []
  			data_bossgame = parsed_json
  		end
  		render json: [{'data_ingame':data_ingame ,'data_bossgame':data_bossgame, 'present': gc.to_i == 0 ? "no" : "yes"}]
	end

	def itemlist
		item = GetCoinFromSpesificItem.where(is_active: "Yes").order(id: :desc)
    	render json: item
	end

	def variables
		vars = Variable.order(id: :desc)
    	render json: vars
	end

	def coinsetting
		gc = GameConfiguration.all
    	render json: gc
	end

	def user_increase_exp
		gc = Member.where(game_code: params[:gc])
		if gc.count > 0 then
			exp = 0;
			gc.each do |c|
				exp = c.exp
			end
			sum =  exp.to_i + 1
			udi = Member.find_by({ :game_code => params[:gc] })
			udi.exp = sum
			udi.save
			render json: [{'game_code':params[:gc], 'current_exp':sum.to_i}]
    	else
    		render json: []
    	end
	end

	def leaderboard_coins
		topPlayer = Member.order(coins: :desc)
    	paginate json: topPlayer, per_page: 10
	end

	def leaderboard_exp
		topPlayer = Member.order(exp: :desc)
    	paginate json: topPlayer, per_page: 10
	end
end
